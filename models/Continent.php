<?php

namespace app\models;
use yii\db\ActiveRecord;

class Continent extends ActiveRecord
{
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['continent_id' => 'continent_id']);
    }
}

