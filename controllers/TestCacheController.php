<?php


namespace app\controllers;


use app\models\Continent;
use Yii;
use yii\filters\HttpCache;
use yii\web\Controller;

class TestCacheController extends Controller
{
    public function behaviors()
    {
        return [

            [
                'class' => 'yii\filters\PageCache',
                'only' => ['task05'],
                'duration' => 40,
                'variations' => [
                    \Yii::$app->language,
                ],
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT COUNT(*) FROM continent',
                ],
            ],
            [
                'class' => 'yii\filters\HttpCache',
                'only' => ['task06'],
                'etagSeed' => function ($action, $params) {
                    $continent = Continent::findOne(['code' => \Yii::$app->request->get('code')]);
                    return serialize([$continent->name, $continent->description]);
                },
                'lastModified' => function ($action, $params) {
                    return 8;
                },

            ],

        ];
    }

    public function actionClearCache()
    {
        $cache = Yii::$app->cache;
        $cache->flush();
    }


    public function actionTask01()
    {
        //Отримуємо компонент $cache
        $cache = Yii::$app->cache;
        //Встановлюємо дані в кеш (при необхідності) і отримуємо змінну
        $title = $cache->getOrSet('title', function() {
            return "Continents of Earth";
        }, 15);
        return $this->render('task01', ['title' => $title]);
    }

    public function actionTask02()
    {
        $cache = Yii::$app->cache;
        $time = $cache->getOrSet('time', function() {
            return time();
        }, 10);
        return $this->render('task02', ['time' => $time]);
    }


    public function actionTask03()
    {
        $cache = Yii::$app->cache;
        //$cache->flush();
        $continents = Yii::$app->db->cache(function($db) {
            return Continent::find()->asArray()->all();
        }, 40);

        return $this->render('task03', [
            'continents' => $continents
        ]);
    }

    public function actionTask05()
    {

        $continents = Continent::find()->asArray()->all();
        return $this->render('task05', [
            'continents' => $continents
        ]);
    }

    public function actionTask06($code = "AF")
    {

//        $continent = Continent::findOne(['code' => $code]);
//        return $this->render('task06', [
//            'continent' => $continent
//        ]);
    }

}
