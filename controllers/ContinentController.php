<?php

namespace app\controllers;
use Yii;
use app\models\Continent;
use app\models\Country;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ContinentController extends Controller
{

    public function actionIndex()
    {
        $continents = Continent::find()->asArray()->all();

        return $this->render('index', [
            'continents' => $continents,
        ]);
    }

    public function actionView($code = "AF")
    {
        //Отримання даних про континент
        $continent = Continent::findOne(['code' => $code]);

        $continents = Continent::find()->all();

        //Отримання даних про країни даного континенту
        //Формування постачальника даних
        $countriesDataProvider = new ActiveDataProvider([
            'query' => Country::find()->where(['continent_id' => $continent->continent_id]),
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);

        //Передача даних в шаблон вигляду
        return $this->render('view', compact(
            'continent',
            'countriesDataProvider',
            'continents'
        ));
    }

//public function actionView($code = "AF")
//{
//    $continent = Continent::findOne(['code' => $code]);
//    return $this->render('view1', [
//       'continent' => $continent
//    ]);
//}

    public function actionAllContinentsDemo()
    {
        $sql = 'SELECT code, name FROM continent';
        $continents = Yii::$app->db->createCommand($sql)->queryAll();
    }

    public function actionOneContinentDemo()
    {
        $sql = 'SELECT code, name FROM continent';
        $continent = Yii::$app->db->createCommand($sql)->queryOne();
        var_dump($continent);
        die;
    }

    public function actionOneColumnDemo()
    {
        $sql = 'SELECT name, code FROM continent';
        $continents = Yii::$app->db->createCommand($sql)->queryColumn();
    }

    public function actionCountDemo()
    {
        $sql = 'SELECT COUNT(*) FROM continent';
        $count = Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function actionCountriesParamsDemo()
    {

        $sql = 'SELECT name, area FROM country WHERE continent_id=:id AND area>:min_area';

        $largeCountriesInEurope = Yii::$app->db->createCommand($sql)
            ->bindValue('id', 4)
            ->bindValue('min_area', 500000)
            ->queryAll();
    }

    public function actionCountriesSerialDemo()
    {
        //Підготовка запиту
        $sql = 'SELECT name FROM country WHERE continent_id=:id';
        $command = Yii::$app->db->createCommand($sql);
        //Серійне виконання запитів
        $countriesInAsia = $command->bindValue('id', 3)->queryAll();
        $countriesInEurope = $command->bindValue('id', 4)->queryAll();
    }

    public function actionCountriesParamsArrayDemo()
    {
        $sql = 'SELECT name, area FROM country WHERE continent_id=:id AND area>:min_area';

        //Формуємо масив параметрів
        $params = ['id' => 4, 'min_area' => 500000];

        //Параметри передаємо в метод createCommand
        $largeCountriesInEurope = Yii::$app->db->createCommand($sql, $params)
            ->queryAll();

        print_r($largeCountriesInEurope);
        die;
    }

    public function actionCountriesBindParams()
    {
        $sql = 'SELECT name FROM country WHERE country_id=:id';
        $command = Yii::$app->db->createCommand($sql)->bindParam('id', $id);
        $id = 227;
        $country[] = $command->queryOne();
        $id = 230;
        $country[] = $command->queryOne();

        print_r($country);
        die;
    }

    public function actionCountriesCount()
    {
        $rows = (new \yii\db\Query())
            ->select(['COUNT(*) AS countryCount', 'continent.name'])
            ->from('country')
            ->leftJoin('continent', 'country.continent_id=continent.continent_id')
            ->groupBy('country.continent_id')
            ->all();
        print_r($rows);
        die;
    }

    protected function findModel($code)
    {
        if (($model = Continent::find()->where(['code' => $code])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}

