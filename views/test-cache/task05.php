<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "List of continents";
?>
<section id="continent-list">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="row">
            <div class="list-group">
                <?php foreach($continents as $continentItem): ?>
                    <div class="list-group-item">
                        <?= Html::encode($continentItem['name']); ?>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>

    </div>


</section>

