<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;use yii\widgets\ListView;
$this->title = $continent->name;
$this->params['breadcrumbs'][] = ['label' => 'Continents', 'url' => ['continent/index']];
$this->params['breadcrumbs'][] = ['label' => $continent['name'], 'url' => ['continent/view', 'code' => $continent['code']]];
?>

<section id="continent-view">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>
        <p><?= Html::encode($continent->description) ?></p>


        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ($this->beginCache('continentSidebar' . $continent->continent_id, ['duration' => 30])): ?>
                    <div class="list-group">
                        <?php foreach($continents as $continentItem): ?>
                        <?php $active = ($continent->continent_id == $continentItem->continent_id) ? "active" : ""; ?>
                            <a href="<?= Url::to(['continent/view', 'code' => $continentItem->code]); ?>" class="list-group-item <?= $active ?>">
                                <?= $continentItem->name ?>
                            </a>
                        <?php endforeach ?>
                    </div>
                    <?php $this->endCache(); ?>
                <?php endif; ?>

            </div>
            <div class="col-12 col-md-6 col-lg-7">
                <?= ListView::widget([
                    'dataProvider' => $countriesDataProvider,
                    'itemView' => '_country',
                    'layout' => "{items}\n{summary}\n{pager}",
                    'options' => [
                        'class' => 'list-group'
                    ],
                    'itemOptions' => [
                        'tag' => false,
                    ],
                ]); ?>
            </div>
        </div>





    </div>
</section>
