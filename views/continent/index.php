<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Список континентів";
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['continent/index']];
?>
<section id="continent-list">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="row">
            <?php foreach($continents as $continentItem): ?>
                <div class="col-md-6 col-lg-4">
                    <div class="card p-4">
                        <div class="card-body">
                            <h3 class="card-title">
                                <a href="<?= Url::to(['continent/view', 'code' => $continentItem['code']]); ?>">
                                    <?= Html::encode($continentItem['name']); ?>
                                </a>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <?= Html::img('@web/images/continents/' . $continentItem['code'] . '.png', ['alt' => $continentItem['code'], 'width' => '100%']) ?>
                        </div>
                        <div class="panel-footer" style="min-height: 100px">
                            <p>
                                <?= Html::encode($continentItem['description']); ?>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

    </div>


</section>
